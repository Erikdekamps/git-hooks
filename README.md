Git Pre-Commit Hook Setup:
-------------------------

1. Rename pre-commit.sample to pre-commit. The file is located in {GitRootDir}/.git/hooks

2. Replace the content with the attached file.

3. Make sure that the file has an execute permission.

```
$ sudo chmod +x /path/to/.git/hooks/pre-commit
```

Requirements:
-------------

You need to have a Code Sniffer installed before using this pre-commit script. To install.

For Debian-based systems:

```
$ sudo apt-get install php-pear
```

For Mac OS:

http://jason.pureconcepts.net/2012/10/install-pear-pecl-mac-os-x/

Or see the general installation guidelines:

http://pear.php.net/manual/en/installation.getting.php

```
$ sudo pear update-channels
$ sudo pear install PHP_CodeSniffer
$ sudo ln -sv /path/to/coder/coder_sniffer/Drupal $(pear config-get php_dir)/PHP/CodeSniffer/Standards/Drupal
```

Basically, the idea in #4 is to link/include the Drupal's code sniffer module to the standard PHP Code Sniffer. Here is a sample/actual command for #4:

```
$ sudo ln -sv /home/ranelpadon/dev/cnngod7/html/sites/all/modules/contribs/coder/coder_sniffer/Drupal $(pear config-get php_dir)/PHP/CodeSniffer/Standards/Drupal
```

The <strong>$(pear config-get php_dir)</strong> part in the #4 command will be usually evaluated in Ubuntu as <strong>/usr/share/php</strong>

To make your Drupal code sniffer module global and not dependent on any Drupal project you can put the module in Drush home folder.
```
$ cd ~/.drush
$ drush dl coder
$ sudo ln -sv ~/.drush/coder/coder_sniffer/Drupal $(pear config-get php_dir)/PHP/CodeSniffer/Standards/Drupal
```

Usage:
--------

The <strong>pre-commit</strong> hook will only run when using <strong>git commit</strong>

If the commit contains any violations of the following type:

1. PHP syntax error
2. PHP coding standard
3. JavaScript coding standard
4. CSS coding standard

it will exit before the commit get indexed and will display the offending file(s).

it will display the line of code and the filename that contain bad code. The developer must resolve the problem first in order to stage the commit.

If you're really sure that it is ok to commit the changes without resolving the coding standard problem detected by the script, you can skip or bypass the validation by using <strong>--no-verify</strong>

```
$ git commit -am "#1452 Commit message | Bypassing validation process" --no-verify
```

